// Includes
#include "config.h"
#include <DHTesp.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <MySQL_Connection.h>
#include <MySQL_Cursor.h>
#include <WiFiClient.h>

// Wifi configuration
const char* ssid = WIFI_NAME;
const char* password = WIFI_PASS;
WiFiClient client;

// Timer helpers
unsigned long startMillis;
unsigned long currentMillis;
const unsigned long period = 60000;   //60 seconds

// MySQL details
IPAddress server_addr(MYSQL_HOST);    // IP of the MySQL *server* here
char mysql_user[] = MYSQL_USER;       // MySQL user login username
char mysql_password[] = MYSQL_PASS;   // MySQL user login password
MySQL_Connection conn((Client *)&client);

// Others
ESP8266WebServer server(80);
DHTesp dht_d3;
DHTesp dht_d4;
const int led = 13;

struct data {
  String dht11_d3_status;
  int dht11_d3_humidity;
  int dht11_d3_temperature;
  float dht11_d3_heatindex;
  String dht11_d4_status;
  int dht11_d4_humidity;
  int dht11_d4_temperature;
  float dht11_d4_heatindex;
  int mq2_a0;
};

void setup()
{
  pinMode(led, OUTPUT);
  digitalWrite(led, 0);
  Serial.begin(115200);

  dht_d3.setup(D3, DHTesp::DHT11);
  dht_d4.setup(D4, DHTesp::DHT11);

  // Wait for connection
  Serial.print("Connecting to WiFi");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Setup mDNS name
  if (MDNS.begin("sensors")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);
  server.begin();
  Serial.println("HTTP server started");
}

void loop()
{
  currentMillis = millis();  //get the current "time" (actually the number of milliseconds since the program started)

  server.handleClient();
  MDNS.update();

  if (currentMillis - startMillis >= period) { //test whether the period has elapsed
    Serial.println("Recording data.");
    recordSensorData();
    startMillis = currentMillis;
  }
}

struct data getSensorData() {
  struct data data_get;

  data_get.dht11_d3_status =  dht_d3.getStatusString();
  data_get.dht11_d3_humidity = dht_d3.getHumidity();
  data_get.dht11_d3_temperature = dht_d3.getTemperature();
  data_get.dht11_d3_heatindex = dht_d3.computeHeatIndex(data_get.dht11_d3_temperature, data_get.dht11_d3_humidity, false);
  data_get.dht11_d4_status =  dht_d4.getStatusString();
  data_get.dht11_d4_humidity = dht_d4.getHumidity();
  data_get.dht11_d4_temperature = dht_d4.getTemperature();
  data_get.dht11_d4_heatindex = dht_d4.computeHeatIndex(data_get.dht11_d4_temperature, data_get.dht11_d4_humidity, false);
  data_get.mq2_a0 = analogRead(A0);

  Serial.println("D3 \tStatus\tHumidity (%)\tTemperature (C)\tHeatIndex (C)");
  Serial.print("\t" + data_get.dht11_d3_status); Serial.print("\t");
  Serial.print(data_get.dht11_d3_humidity, 1); Serial.print("\t\t");
  Serial.print(data_get.dht11_d3_temperature, 1); Serial.print("\t\t");
  Serial.print(data_get.dht11_d3_heatindex, 1); Serial.println("");
  Serial.println("D4 \tStatus\tHumidity (%)\tTemperature (C)\tHeatIndex (C)");
  Serial.print("\t" + data_get.dht11_d4_status); Serial.print("\t");
  Serial.print(data_get.dht11_d4_humidity, 1); Serial.print("\t\t");
  Serial.print(data_get.dht11_d4_temperature, 1); Serial.print("\t\t");
  Serial.print(data_get.dht11_d4_heatindex, 1); Serial.println("");
  Serial.println("MQ-2\tValue");
  Serial.println("\t" + String(data_get.mq2_a0));

  return data_get;
}

void recordSensorData() {
  char INSERT_SQL[] = "INSERT INTO temperature.mcu_sensors(dht11_d4_humidity,dht11_d4_temperature,mq2_a0,dht11_d3_humidity,dht11_d3_temperature) VALUES (%d,%d,%d,%d,%d)";
  char query[256];

  Serial.println("Connecting to database...");
  if (conn.connect(server_addr, 3306, mysql_user, mysql_password)) {
    Serial.println("Inserting.");

    data data_get = getSensorData();
    sprintf(query, INSERT_SQL, data_get.dht11_d4_humidity, data_get.dht11_d4_temperature, data_get.mq2_a0, data_get.dht11_d3_humidity, data_get.dht11_d3_temperature);

    MySQL_Cursor *cur_mem = new MySQL_Cursor(&conn);
    cur_mem->execute(query);
    delete cur_mem;
    conn.close();
  }
}

void handleRoot() {
  digitalWrite(led, 1);

  data data_get = getSensorData();

  String response = "";
  response += "d3_dhtstatus: " + String(data_get.dht11_d3_status);
  response += "\nd3_humidity: " + String(data_get.dht11_d3_humidity);
  response += "\nd3_temperature: " + String(data_get.dht11_d3_temperature);
  response += "\nd3_heatindex: " + String(data_get.dht11_d3_heatindex);
  response += "\n\nd4_dhtstatus: " + String(data_get.dht11_d4_status);
  response += "\nd4_humidity: " + String(data_get.dht11_d4_humidity);
  response += "\nd4_temperature: " + String(data_get.dht11_d4_temperature);
  response += "\nd4_heatindex: " + String(data_get.dht11_d4_heatindex);
  response += "\n\nmq2: " + String(data_get.mq2_a0);

  server.send(200, "text/plain", response);

  digitalWrite(led, 0);
}